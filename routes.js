var controler = require('./controler')

module.exports = function (app) {
   app.route('/create')
      .get(controler.creator);

   app.route('/infos')
      .get(controler.getInfos);

   app.route('/reqCPU')
      .get(controler.reqCPU);

   app.route('/reqRAM')
      .get(controler.reqRAM);

   app.route('/reqCM')
      .get(controler.reqCM);

   app.route('/reqHDD')
      .get(controler.reqHDD);

   app.route('/reqSSD')
      .get(controler.reqSSD);

   app.route('/reqBoitier')
      .get(controler.reqBoitier);

   app.route('/req')
      .post(controler.request);

      app.route('/index')
      .get(controler.myindexinformations);

   app.route('/removeData')
      .post(controler.removeData);

}