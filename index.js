const express = require('express');
const app = express();
const assert = require('assert');


var routes = require('./routes');
const bodyParser= require('body-parser')


app.use(bodyParser.urlencoded({extended: true}))

routes(app);

app.listen(3000, function() {
   console.log('listening on 3000')
 })