 const url = 'mongodb://localhost:27017';
const dbName = 'DBMateriel';
const colName = 'composants'
const assert = require('assert');
const MongoClient = require('mongodb').MongoClient
var faker = require('faker');


exports.creator = ((req, res) => {

   // connexion à la base de donnée MANGO
   MongoClient.connect(url, function (err, client) {
      if (err) res.json({
         err: err
      });
      const db = client.db(dbName);


      var messsage = "Collection has been deleted !"

      // Suppresion de la collection DBMaterie

      db.listCollections().toArray(function (err, collInfos) {
         collInfos.forEach(element => {
            if (element["name"] == colName) {
               db.dropCollection(colName, function (err, delOK) {
                  if (err) throw err; 
                  console.log(delOK);
                  // res.json({
                  //    resultat: messsage
                  // });
               });
            }
         });
      });

      // Creation de la collection DBMateriel
      db.createCollection(colName, {
         validator: {
            $jsonSchema: {
               bsonType: "object",
               required: ["type", "prix", "poids", "garantie", "nom"],
               properties: {
                  type: {
                     enum: ["Carte mère", "Boitier", "SSD", "Disque dur", "RAM", "CPU", "GPU", "Alim"],
                     description: "Le type du component"
                  },
                  prix: {
                     bsonType: "decimal",
                     description: "Le prix du component"
                  },
                  poids: {
                     bsonType: "decimal",
                     description: "Le poids du component"
                  },
                  garantie: {
                     bsonType: "int",
                     description: "La garantie du component en années"
                  },
                  nom: {
                     bsonType: "string",
                     description: "Le nom du component"
                  },
                  frequence: {
                     bsonType: "decimal",
                     description: "La fréquence (CPU, GPU, RAM)"
                  },
                  chipset: {
                     bsonType: "string",
                     description: "Le chipset (CPU, CM)"
                  },
                  gpuIntegre: {
                     bsonType: "bool",
                     description: "Si le CPU contient un GPU intégré ?"
                  },
                  taille: {
                     bsonType: "string",
                     description: "La taille de la carte mère"
                  },
                  connectique: {
                     bsonType: "string",
                     description: "connectiques de la carte mère"
                  },
                  typeConnexion: {
                     enum: ["SATA III", "SATA II", "PCI-Express"],
                     description: "Le type de connexion (SSD, HDD)"
                  },
                  vitesseTransfert: {
                     bsonType: "decimal",
                     description: "La vitesse de trasnfert (SSD, HDD)"
                  },
                  vitesseLecture: {
                     bsonType: "decimal",
                     description: "La vitesse de lecture (SSD, HDD)"
                  },
                  stockage: {
                     bsonType: "int",
                     description: "Le stockage (SSD, HDD)"
                  },
                  microATX: {
                     bsonType: "bool",
                     description: "microATX disponible ?"
                  },
                  atx: {
                     bsonType: "bool",
                     description: "ATX disponible ?"
                  },
                  hauteur: {
                     bsonType: "decimal",
                     description: "La hauteur du boitier"
                  },
                  largeur: {
                     bsonType: "decimal",
                     description: "La largeur du boitier"
                  },
                  profondeur: {
                     bsonType: "decimal",
                     description: "La profondeur du boitier"
                  },
                  materiau: {
                     bsonType: "string",
                     description: "Le matériau utilisé pour le boitier"
                  },
                  puissance: {
                     bsonType: "int",
                     description: "La puissance de l'alimentation"
                  }
               }
            }
         }
      });


      insertData().then(function (result) {
         res.json({
            success: result,
         });
      });
      //ajout des donnees fake
      // res.json({
      //    success: 'test'
      // });
   });
})

exports.getInfos = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      assert.equal(null, err);
      console.log("Connected successfully to server");

      const db = client.db(dbName);

      db.listCollections().toArray(function (err, collInfos) {
         collInfos.forEach(element => {
            console.log(element["name"])
         });
         res.json({
            DB: collInfos
         })
      });
   })
})

//try catch index infos
exports.myindexinformations = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      assert.equal(null, err);
      console.log("Connected successfully to server");

      const db = client.db(dbName);

      db.collection(colName).indexInformation(function (err, result) {

         db.collection(colName).indexExists("type", function (err, ress) {
            res.json({
               resultatt: ress,
               resultat: result
            });
         });
      });
   });
});

exports.request = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      const db = client.db(dbName);
      //dbName.colName.createIndex({CM:1})
      //db.collection(colName).find({}).toArray(function (err, result) {
      var query = {
         type: req.body.type
      }
      db.collection(colName).createIndex({
         type: 1
      })
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result: result
         })
      })
   })
})

exports.reqBoitier = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      const db = client.db(dbName);
      var query = {
         type: {
            type: "Boitier"
         }
      }
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result: result
         })
      });
   })
})

exports.reqCM = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      const db = client.db(dbName);
      var query = {
         type: "Carte mère"
      }
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result: result
         })
      });
   })
})

exports.reqCPU = ((req, res) => {
   MongoClient.connect(url, function (err, client) {

      const db = client.db(dbName);
      var query = {
         type: "CPU"
      }
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result: result
         })

      });
   })
})

exports.reqRAM = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      const db = client.db(dbName);
      var query = {
         type: "RAM"
      }
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result: result
         })
      });
   })
})

exports.reqHDD = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      const db = client.db(dbName);
      var query = {
         type: "HDD"
      }
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result: result
         })
      });
   })
})

exports.reqSSD = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      const db = client.db(dbName);
      var query = {
         type: "SSD"
      }
      db.collection(colName).find(query).toArray(function (err, result) {

         if (err) res.json({
            err: err
         });
         res.json({
            res: result.length,
            result
         })
      });
   })
})


function getRandomInt(max) {
   return Math.floor(Math.random() * Math.floor(max));
}

function insertData() {
   return new Promise(function (resolve, reject) {

      MongoClient.connect(url, function (err, client) {
         if (err) reject(err)
         const db = client.db(dbName);

         for (var i = 0; i < 10000; i++) {



            switch (0) {
               case (0):
                  var myCPU = {
                     type: "CPU",
                     prix: "",
                     poids: faker.random.number(),
                     garantie: 2,
                     nom: "Intel i7 7800k",
                     frequence: 3.6,
                     chipset: "1151",
                     gpuIntegre: faker.random.boolean()
                  };
                  var myGPU = {
                     type: "GPU",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "GTX 1070",
                     frequence: 3,
                  };
                  var myRAM = {
                     type: "RAM",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "GSKILL",
                     frequence: 3,
                  };
                  var myCM = {
                     type: "Carte mère",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "Asus Rog",
                     chipset: '1151',
                     taille: 'ATX',
                     connectique: 'HDMI, VGA, USB-C'
                  };
                  var mySSD = {
                     type: "SSD",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "Samsung ",
                     taille: 'ATX',
                     connectique: 'HDMI, VGA, USB-C'
                  };
                  var myHDD = {
                     type: "HDD",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "Western Digital",
                     typeConnexion: 'SATA-2',
                     vitesseTransfert: '100Mb/s',
                     vitesseLecture: '200Mb/s',
                     stockage: '2000'
                  };
                  var myBoitier = {
                     type: "Boitier",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "In Win",
                     formatCM: 'ATX',
                     tailleBoitier: 'Grand',
                     Materiau: 'Acier'
                  };
                  var myAlim = {
                     type: "ALIM",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "In Win",
                     puissance: '300',
                     format: 'micro-ATX',
                  };
                  break;

               case (1):
                  var myCPU = {
                     type: "CPU",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 5,
                     nom: "Intel i7 9900k",
                     frequence: 5.6,
                     chipset: "1151",
                     gpuIntegre: faker.random.boolean()
                  };
                  var myGPU = {
                     type: "GPU",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 5,
                     nom: "GTX 1080",
                     frequence: 4,
                  };
                  var myRAM = {
                     type: "RAM",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "CRUCIAL",
                     frequence: 2,
                  };
                  var myCM = {
                     type: "Carte mère",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "MSI GAMER",
                     chipset: '1151',
                     taille: 'micro-ATX',
                     connectique: 'HDMI, VGA, USB-C'
                  };
                  var mySSD = {
                     type: "SSD",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "SANDISK",
                     taille: 'ATX',
                     connectique: 'HDMI, VGA, USB-C'
                  };
                  var myHDD = {
                     type: "HDD",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "Western Digital",
                     typeConnexion: 'SATA-3',
                     vitesseTransfert: '200Mb/s',
                     vitesseLecture: '400Mb/s',
                     stockage: '4000'
                  };
                  var myBoitier = {
                     type: "Boitier",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "BE QUIET",
                     formatCM: 'ATX',
                     tailleBoitier: 'moyen',
                     Materiau: 'Acier'
                  };
                  var myAlim = {
                     type: "ALIM",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "In Win",
                     puissance: '600',
                     format: 'ATX',
                  };
                  break;
               case (2):
                  var myCPU = {
                     type: "CPU",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 2,
                     nom: "Intel i3 4600",
                     frequence: 3.6,
                     chipset: "1151",
                     gpuIntegre: faker.random.boolean()
                  };
                  var myGPU = {
                     type: "GPU",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "GTX 1060",
                     frequence: 1,
                  };
                  var myRAM = {
                     type: "RAM",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "CORSAIR",
                     frequence: 4,
                  };
                  var myCM = {
                     type: "Carte mère",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "Asus Rog Strix",
                     chipset: '1151',
                     taille: 'ATX',
                     connectique: 'HDMI, VGA, USB-C'
                  };
                  var mySSD = {
                     type: "SSD",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "O",
                     chipset: '1151',
                     taille: 'ATX',
                     connectique: 'HDMI, VGA, USB-C'
                  };
                  var myHDD = {
                     type: "HDD",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "SEAGATE",
                     typeConnexion: 'SATA-3',
                     vitesseTransfert: '120Mb/s',
                     vitesseLecture: '180Mb/s',
                     stockage: '1500'
                  };
                  var myBoitier = {
                     type: "Boitier",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "LIAN LI",
                     formatCM: 'E-ATX',
                     tailleBoitier: 'Grand',
                     Materiau: 'ALUMINIUM'
                  };
                  var myAlim = {
                     type: "ALIM",
                     prix: faker.commerce.price(),
                     poids: faker.random.number(),
                     garantie: 3,
                     nom: "In Win",
                     puissance: '700',
                     format: 'ATX',
                  };
                  break;
            }
            db.collection(colName).insertMany([myCPU, myGPU, myRAM, myCM, mySSD, myHDD, myBoitier, myAlim], function (error, result) {
               if (error) reject(error)
            });

            if (i == 9999) {
               resolve("Insertions terminées");
            }
         }
      });
   }); //promise
}

exports.removeData = ((req, res) => {
   MongoClient.connect(url, function (err, client) {
      if (err) res.json({
         err: err
      });
      const db = client.db(dbName);
      var myquery = {
         type: req.body.type
      };
      db.collection(colName).deleteMany(myquery, function (error, result) {
         if (error) res.json({
            err: err
         });
         res.json({
            res: result
         })
      });

   })
})