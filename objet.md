caraCommune
{
   type : ["Carte mere", "Boitier", "SSD", "Disque dur", "RAM", "CPU", "GPU", "Alimentation"], 
   prix : number, 
   poids: number, 
   garantie : number, 
   nom : string;
}

CPU : caraCommune 
{
   frequence: number, 
   chipset: string, 
   gpuIntegre: boolean,
}

RAM : caraCommune 
{
   frequence: number
}

GPU : caraCommune 
{
   frequence: number
}

CM: caraCommune
{
   chipset : string,
   taille: string, 
   connectique: string
}

SSD : caraCommune 
{
   typeConnexion: ["SATA III", "SATA II","PCI-Express"],
   vitesseTransfere: float,
   vitesseLecture: float,
   stockage: int
}

DD : caraCommune
{
   typeConnexion: ["SATA III", "SATA II","PCI-Express"],
   vitesseTransfere: float,
   vitesseLecture: float,
   stockage: int
}

boitier : caraCommune
{
   microATX: boolean,
   ATX: boolean,
   hauteur: number,
   largeur: number,
   profondeur : number,
   materiau : string
}

alim : caraCommune
{
   puissance: number
}